
<?php

function t_init_assets(){
    wp_enqueue_script('global-js', get_stylesheet_directory_uri() . '/assets/js/toannt/global.js', array('jquery'), '1.0', true);
}

function t_process_login(){
	if ( isset( $_POST['login'], $_POST['username'], $_POST['password'] ) ) {

		try{
			$creds = array(
				'user_login'    => $_POST['username'], // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
				'user_password' => $_POST['password'], // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.ValidatedSanitizedInput.MissingUnslash
			);

			if ( empty( $creds['user_login'] ) ) {
				throw new Exception( "Vui lòng không bỏ trống tên đăng nhập" );
			}

            if ( empty( $creds['user_password'] ) ) {
				throw new Exception( "Vui lòng không bỏ trống mật khẩu" );
			}
			// Perform the login.
			$user = wp_signon( $creds, is_ssl() );
			
			if ( is_wp_error( $user ) ) {
				throw new Exception( "Tài khoản hoặc mật khẩu không đúng, vui lòng kiểm tra lại" );
			} else {
				$redirect = wc_get_page_permalink( 'myaccount' );
				wp_redirect( wp_validate_redirect( apply_filters( 'woocommerce_login_redirect', remove_query_arg( 'wc_error', $redirect ), $user ), $redirect ) ); // phpcs:ignore
				exit;
			}
		} catch ( Exception $e ) {
			wc_add_notice( apply_filters( 'login_errors', $e->getMessage() ), 'error' );
		}
	}
}

function t_process_register(){
    if (isset($_POST['register'], $_POST['username'], $_POST['password'])){
        try{
            $email = $_POST['email'];
            $username = $_POST['username'];
            $password = $_POST['password'];
            if (!username_exists($username) && !email_exists($email)) {
                $userdata = array(
                    'user_login' => $username,
                    'user_email' => $email,
                    'user_pass' => $password
                );
                $user_id = wp_insert_user($userdata);

                $creds = array(
                    'user_login'    => $username, // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
                    'user_password' => $password, // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.ValidatedSanitizedInput.MissingUnslash
                );

                $user = wp_signon( $creds, is_ssl() );

                if ($user_id)
                {
                    $redirect = wc_get_page_permalink( 'myaccount' );
                    wp_redirect( wp_validate_redirect( apply_filters( 'woocommerce_login_redirect', remove_query_arg( 'wc_error', $redirect ), $user ), $redirect ) ); // phpcs:ignore
                    exit;
                    ?>
                        <div class="form-register-susscess">
                            <h3>Chào thành viên mới của Kizuna</h3>
                        </div>
                        <script>
                            setTimeout(()=>window.location = "<?php echo wc_get_page_permalink( 'myaccount' ); ?>",3000)
                        </script>
                    <?php
                    exit;
                }
            }
            else {
                throw new Exception( "Số điện thoại hoặc địa chỉ email đã được đăng ký, vui lòng thay kiểm tra lại." );
            }
        } catch ( Exception $e ) {
            wc_add_notice( $e->getMessage(), 'error' );
        }
    }
}

function _woocommerce_my_account_register() {
    include 'woocommerce/myaccount/form-register.php';
} 

function _show_products_by_categories(){
    include 'woocommerce/custom/products-by-categories.php';
}

function my_query_post_type($query) {
    if ( is_category() && ( ! isset( $query->query_vars['suppress_filters'] ) || false == $query->query_vars['suppress_filters'] ) ) {
        $query->set( 'post_type', array( 'post', 'cook' ) );
        return $query;
    }
}


function filter_category(){
    if(isset($_POST['product_cat'])) $args = array(
        'post_type'      => 'product',
        'posts_per_page' => 8,
        'product_cat'    => $_POST['product_cat']
    );    
    else $args = array(
        'post_type'      => 'product',
        'posts_per_page' => 8,
    );
    
    $loop = new WP_Query( $args );
    echo "<div class='products row row-small large-columns-4 medium-columns-3 small-columns-2'>";
        while ( $loop->have_posts() ) : $loop->the_post();
            global $product;
                wc_get_template('content-product.php');
        endwhile;
    echo "</div>";
    wp_reset_postdata();
    exit;
}

add_filter('pre_get_posts', 'my_query_post_type');
add_action('init', 't_init_assets');
add_action('wp_loaded', 't_process_login');
add_action('wp_loaded', 't_process_register');
add_action( 'wp_ajax_filter_category', 'filter_category' );
add_action( 'wp_ajax_nopriv_filter_category', 'filter_category' );

add_shortcode('show_products_by_categories', '_show_products_by_categories');
add_shortcode('woocommerce_my_account_register', '_woocommerce_my_account_register');
?>