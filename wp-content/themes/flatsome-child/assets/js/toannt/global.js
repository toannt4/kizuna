function debounce(func, timeout = 300) {
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      func.apply(this, args);
    }, timeout);
  };
}

jQuery(document).ready(function () {
  jQuery(".form-input-password").append(
    `<div class="form-input-password-icon"></div>`
  );
  jQuery(".form-input-password-icon").click(function () {
    let input = jQuery(this).prev();
    let type = input.attr("type");
    if (type == "password") input.attr("type", "text");
    else input.attr("type", "password");
  });
});
