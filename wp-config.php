<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'kizuna' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'M+i7VXDZ>r>Uh/;267++D;o{~QuB<(E(i&}.*ox#XH(:8o>p0fUHt%V+iJVk65%}' );
define( 'SECURE_AUTH_KEY',  '?K$:{zo,tOj!]}}7yXaS*Yck$(v ]##-qhH3.#-$.b2R<JuqM+y7j*iIh8DGvt|[' );
define( 'LOGGED_IN_KEY',    'ICZf^J7$L=)O=B3(&80h`LPJ}^txJb;w.R%Mh)W)&hmU-[+d[5nF>R~V_Zlq_ml2' );
define( 'NONCE_KEY',        'u+.j*u8era@tck:_zO|G7HlG8_C][D@^+/sJA&GOtmxq=fpr;?^GPEPO#yMim9tu' );
define( 'AUTH_SALT',        'ECp,z4LCjjJ4K7ZLf>K``]Yr8bLD>% _`=v-Qd%#o`:s:Pd1+$5;cfeY7r6p_y73' );
define( 'SECURE_AUTH_SALT', '* dr{sOc#&_kq_C:?]](pL2MfQp_c9FHThAxM/qiV_8zwS3&]r:VQ<x@jL?>VYSn' );
define( 'LOGGED_IN_SALT',   '0>0^0X*L6$;=`7X9u_@(#%HML|_9A@|n$.o On57,^Db~x}E8G/<fZ<=6ADn,g|b' );
define( 'NONCE_SALT',       '3Y#NF,-wbk=]q7Cv#*S1iMcc=0pvHmWm;d9eM_GYp]L5gFc0h;gt3tp`&D2Y9er;' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
