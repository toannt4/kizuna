<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 4.1.0
 *
 * @flatsome-parallel-template {
 * form-login-lightbox-left-panel.php
 * form-login-lightbox-right-panel.php
 * }
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<?php 
wp_enqueue_style('form-login-css', get_stylesheet_directory_uri() . '/assets/css/toannt/form-login.css');
get_header();
?>

<div class="form-login-section t-form">
	<div class="row row-collapse align-middle">
		<div class="col large-6 form-login-left">
			<img src="<?php echo get_stylesheet_directory_uri().'/assets/img/form-login/background.png'; ?>" />
		</div>
		<div class="col flex align-middle align-center small-12 medium-12 large-6 form-login-right">
			<div class="form-login-right-box">
				<h2 class="text-center mb">Đăng nhập</h2>
				<form class="mb wc-auth-login" method="post">
					<div class="flex flrb-inputs flex-wrap mb">
						<div class="flrb-input-username w-100">
							<input type="text" placeholder="Nhập số điện thoại hoặc email" required name="username" id="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( $_POST['username'] ) : ''; ?>"  />
						</div>
						<div class="form-input-password w-100">
							<input placeholder="Nhập mật khẩu" class="mb-0" name="password" id="password" required type="password" />
						</div>
						<a id="lost-password">Quên mật khẩu</a>
					</div>
					<div class="flrb-submit wc-auth-actions">
						<button type="submit" name="login" class="button primary w-100" value="Login">Đăng nhập</button>
						<p class="text-center">Bằng cách đăng nhập, bạn đồng ý với<br /> <a>Điều khoản sử dụng</a> và <a>Chính sách quyền riêng tư</a>.</p>
					</div>
				</form>
				<span class="w-100 relative flrb-devide">
					<p>Hoặc tiếp tục với</p>
				</span>
				<div class="flrb-gg mb mt">
					<a class="button flrb-gg-btn is-outline primary w-100">
						<img width="32px" height="32px" src="<?php echo get_stylesheet_directory_uri().'/assets/img/form-login/google.png'; ?>" />
						<p class="mb-0">Đăng nhập</p>
					</a>
				</div>
				<p class="text-center w-100">Không có tài khoản? <a href="dang-ky">Đăng ký</a></p>
			</div>
		</div>
	</div>
</div>

