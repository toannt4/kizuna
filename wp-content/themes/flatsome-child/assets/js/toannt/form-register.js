const otpHtml = 
`
<div class="wrap t-form" id="form-otp">
  <div class="col small-11 medium-8 large-4 wrap-child">
    <div class="form-otp-box t-box">
      <h2 class="text-center">Tạo tài khoản mới</h2>
      <p class="text-center mb">Mã xác minh đã được gửi đến số điện thoại: <a id="form-otp-label-phone"></a><br/>Vui lòng nhập mã xác nhận</p>
      <div class="fo-input-numbers-section w-100 mb">
        <input id="form-otp-input-numbers" placeholder="Nhập mã OTP" class="mb-0" />
      </div>
      <span class="w-100 relative form-otp-resend-section text-center">
        <span class="flex justify-center" id="form-otp-resend-time"><p id="form-otp-resend-countdown" class="mr-0-5">30</p> giây</span>
        <p style="display:none" id="form-otp-resend-button">Không nhận được mã? <a onclick={regetOtp()}>Gửi lại</a></p>
      </span>
      <div class="form-submit">
        <button id="form-otp-button-register" onclick={sendOtp()} class="button primary w-100">Tiếp theo</button>
      </div>		
    </div>
  </div>
</div>
`

const infoHtml = 
`
<div class="wrap t-form" id="form-info">
  <div class="col small-11 medium-8 large-4 wrap-child">
    <div class="form-info-box t-box">
      <h2 class="text-center mb">Tạo tài khoản mới</h2>
      <form method="POST">
        <div class="form-info-inputs w-100 mb">
          <input id="form-info-input-username" type="number" name="username" value="" hidden/>
          <input id="form-info-input-email" type="email" name="email" placeholder="Nhập email" value=""/>
          <div class="form-input-password w-100">
            <input id="form-info-input-password" class="mb-0" type="password" name="password" required placeholder="Nhập mật khẩu" />
          </div>
          <div class="form-input-password w-100">
            <input id="form-info-input-repeat-password" onkeyup={checkRepassword(event)} class="mb-0" required type="password" placeholder="Nhập lại mật khẩu" />
          </div>
        </div>
        <div class="fo-submit">
          <button id="form-info-button-submit" name="register" value="Register" type="submit" disabled class="button primary w-100">Đăng ký</button>
        </div>		
      </form>
    </div>
  </div>
</div>
`

function isVietnamesePhoneNumber(phone) {
  return /(84|0[1-9])+([0-9]{8})\b/.test(phone);
}

function initError(element, message) {
  element.parent().addClass("input-error");
  element.val("");
  element.attr("placeholder", message);
}

function initWrapActions() {
  jQuery(".wrap").click(function () {
    jQuery(this).remove();
    jQuery(".woocommerce-error").remove();
  });
  jQuery(".wrap-child").click(function (e) {
    e.stopPropagation();
  });
}

function getPhoneVal() {
  let phone = jQuery("#form-register-input-phone").val();
  return phone;
}

function getOtpVal() {
  let otp = jQuery("#form-otp-input-numbers").val();
  return otp;
}
/////////////////////////

function sendPhone() {
  const element = jQuery("#form-register-input-phone");
  if (isVietnamesePhoneNumber(getPhoneVal())) {
    getRecaptcha();
  } else
    initError(
      element,
      "Số điện thoại không đúng định dạng, vui lòng kiểm tra lại"
    );
}

function getRecaptcha() {
  if (!window.recaptchaVerifier) {
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
      "recaptcha-container",
      {
        size: "invisible",
      }
    );
  }
  getOtp();
}

function getOtp() {
  const element = jQuery("#form-register-input-phone");
  const phoneNumber = "+84" + getPhoneVal();
  const appVerifier = window.recaptchaVerifier;

  firebase
    .auth()
    .signInWithPhoneNumber(phoneNumber, appVerifier)
    .then((confirmationResult) => {
      window.confirmationResult = confirmationResult;
      showOtpForm();
      setResend();
    })
    .catch((error) => {
      console.log(error);
      initError(element, "Có lỗi xảy ra, vui lòng tải lại trang");
    });
}

function regetOtp() {
  getOtp();
  setResend();
  toggleResend();
}

function sendOtp() {
  if (!window.confirmationResult) return false;
  const element = jQuery("#form-otp-input-numbers");
  const otp = getOtpVal();

  window.confirmationResult
    .confirm(otp)
    .then((result) => {
      showInfoForm();
    })
    .catch((error) => {
      console.log(error);
      initError(element, "Mã OTP không đúng hoặc hết hạn, vui lòng thử lại");
    });
}

function checkRepassword(e) {
  if (e.target.value == jQuery("#form-info-input-password").val())
    jQuery("#form-info-button-submit").removeAttr("disabled");
  else jQuery("#form-info-button-submit").attr("disabled", true);
}

function showOtpForm() {
  jQuery("#form-otp-popup").append(otpHtml);
  jQuery("#form-otp-label-phone").html(getPhoneVal());
  initWrapActions()
}

function showInfoForm() {
  jQuery("#form-otp").remove();
  jQuery("#form-info-popup").append(infoHtml);
  jQuery("#form-info-input-username").val(getPhoneVal());
  initWrapActions()
}

function setResend() {
  window.clearInterval(window.intervalResend);

  let time = 30;
  jQuery("#form-otp-resend-countdown").html(time);
  window.intervalResend = setInterval(() => {
    time--;
    if (time == 0) {
      clearInterval(intervalResend);
      jQuery("#form-otp-resend-countdown").html(0);
      toggleResend();
    } else jQuery("#form-otp-resend-countdown").html(time);
  }, 1000);
}

function toggleResend() {
  let time = jQuery("#form-otp-resend-countdown").html();

  if (time == 0) {
    jQuery("#form-otp-resend-time").hide();
    jQuery("#form-otp-resend-button").show();
  } else {
    jQuery("#form-otp-resend-time").show();
    jQuery("#form-otp-resend-button").hide();
  }
}

jQuery(document).ready(function () {
  initWrapActions();
});
