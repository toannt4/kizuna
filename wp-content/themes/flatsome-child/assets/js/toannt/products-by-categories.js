function ajax_filter(product_cat) {
  jQuery.ajax({
    method: "POST", //Phương thức truyền post hoặc get
    url: ajax_object.ajax_url, //Đường dẫn chứa hàm xử lý dữ liệu. Mặc định của WP như vậy
    data: {
      action: "filter_category",
      dataType: "json", //Dạng dữ liệu trả về xml, json, script, or html
      product_cat: product_cat, //Biến truyền vào xử lý. $_POST['website']
    },
    success: function (result) {
      //Làm gì đó khi dữ liệu đã được xử lý
      jQuery("#show_products_by_filter_category").html(result);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      //Làm gì đó khi có lỗi xảy ra
      alert("Đã có lỗi xảy ra");
      return false;
    },
  });
}

jQuery(document).ready(function () {
  ajax_filter();
  jQuery(".category-button").click(function () {
    product_cat = jQuery(this).attr("product_cat");
    ajax_filter(product_cat);
    return false;
  });
});
