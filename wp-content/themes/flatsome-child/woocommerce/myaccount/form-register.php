<?php
/**
 * Register Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-register.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 7.0.1
 */

 if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<script src="https://www.gstatic.com/firebasejs/9.14.0/firebase-app-compat.js"></script>
<script src="https://www.gstatic.com/firebasejs/9.14.0/firebase-analytics-compat.js"></script>
<script src="https://www.gstatic.com/firebasejs/9.14.0/firebase-auth-compat.js"></script>
<script>
	const firebaseConfig = {
		apiKey: "AIzaSyAs8hgCEHHOjHX_cO-hVeXLRidyJDR9Kpo",
		authDomain: "kizuna-83b8e.firebaseapp.com",
		projectId: "kizuna-83b8e",
		storageBucket: "kizuna-83b8e.appspot.com",
		messagingSenderId: "670197014094",
		appId: "1:670197014094:web:cf42ffbf0e916f5745ad1f",
		measurementId: "G-ET5Z8HD3LN"
	};
	// Initialize Firebase
	const app = firebase.initializeApp(firebaseConfig);
	const analytics = firebase.analytics();
</script>
<?php
	wp_enqueue_style('form-register-css', get_stylesheet_directory_uri() . '/assets/css/toannt/form-register.css');
    wp_enqueue_script('form-register-js', get_stylesheet_directory_uri() . '/assets/js/toannt/form-register.js', array('jquery'), '1.0', true);
	get_header();
?>

<div class="form-register-section t-form" id="form-register">
	<div class="row row-collapse align-middle">
		<div class="col large-6 form-register-left">
			<img src="<?php echo get_stylesheet_directory_uri().'/assets/img/form-register/background.png'; ?>" />
		</div>
		<div class="col flex align-middle align-center small-12 medium-12 large-6 form-register-right">
			<div class="form-register-right-box t-box">
				<h2 class="text-center mb">Tạo tài khoản mới</h2>
				<div class="mb">
					<div class="flex frrb-inputs flex-wrap mb">
						<div class="frrb-input-username w-100">
							<input type="tel" id="form-register-input-phone" class="mb-0" name="phone" value="" placeholder="Nhập số điện thoại" pattern="(84|0[1-9])+([0-9]{8})\b" required>
						</div>
					</div>
					<div class="frrb-submit">
						<button id="form-register-button-register" class="button primary w-100" onclick={sendPhone()}>Đăng ký</button>
						<p class="text-center">Bằng cách đăng ký, bạn đồng ý với<br /> <a>Điều khoản sử dụng</a> và <a>Chính sách quyền riêng tư</a>.</p>
					</div>
				</div>
				<span class="w-100 relative frrb-devide">
					<p>Hoặc đăng ký với</p>
				</span>
				<div class="frrb-gg mb mt">
					<a class="button frrb-gg-btn is-outline primary w-100">
						<img width="32px" height="32px" src="<?php echo get_stylesheet_directory_uri().'/assets/img/form-register/google.png'; ?>" />
						<p class="mb-0">Đăng nhập</p>
					</a>
				</div>
				<p class="text-center w-100">Đã có tài khoản? <a href="tai-khoan">Đăng nhập</a></p>
			</div>
		</div>
	</div>
</div>
<div id="recaptcha-container"></div>

<div id="form-otp-popup"></div>

<?php if(isset($_POST['username'])){ ?>
	<div id="form-info-popup">
		<div class="wrap t-form" id="form-info">
			<div class="col small-11 medium-8 large-4 wrap-child">
				<div class="form-info-box t-box">
					<h2 class="text-center mb">Tạo tài khoản mới</h2>
					<form method="POST">
						<div class="form-info-inputs w-100 mb">
							<input id="form-info-input-username" type="number" name="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( $_POST['username'] ) : ''; ?>" hidden/>
							<input id="form-info-input-email" type="email" name="email" placeholder="Nhập email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( $_POST['email'] ) : ''; ?>"/>
							<div class="form-input-password w-100">
								<input id="form-info-input-password" class="mb-0" type="password" name="password" required placeholder="Nhập mật khẩu" />
							</div>
							<div class="form-input-password w-100">
								<input id="form-info-input-repeat-password" onkeyup={checkRepassword(event)} class="mb-0" required type="password" placeholder="Nhập lại mật khẩu" />
							</div>
						</div>
						<div class="fo-submit">
							<button id="form-info-button-submit" name="register" value="Register" type="submit" disabled class="button primary w-100">Đăng ký</button>
						</div>		
					</form>
				</div>
			</div>
		</div>
	</div> 
<?php } else { ?>
<div id="form-info-popup"></div> 
<?php } ?>