<?php
    wp_register_style( 'products-by-categories-css', get_stylesheet_directory_uri() . '/assets/css/toannt/products-by-categories.css');
    wp_enqueue_style( 'products-by-categories-css');

    wp_register_script('products-by-categories-js', get_stylesheet_directory_uri() . '/assets/js/toannt/products-by-categories.js', array('jquery'), '1.0', true);
    wp_enqueue_script('products-by-categories-js');
    wp_localize_script( 'products-by-categories-js', 'ajax_object', array( 'ajax_url' =>   admin_url( 'admin-ajax.php' ) ) );
?>

<div class="row">
    <div class=" flex col small-12 text-left" style="overflow:auto;">
        <?php
            $taxonomy     = 'product_cat';
            $orderby      = 'name';  
            $show_count   = 0;      // 1 for yes, 0 for no
            $pad_counts   = 0;      // 1 for yes, 0 for no
            $hierarchical = 1;      // 1 for yes, 0 for no  
            $title        = '';  
            $empty        = 1;

            $args = array(
                'taxonomy'     => $taxonomy,
                'orderby'      => $orderby,
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'title_li'     => $title,
                'hide_empty'   => $empty
            );
            $all_categories = get_categories( $args );
            foreach ($all_categories as $cat) {
                echo "<a class='category-button mb-0 button button-primary' product_cat='{$cat->slug}'>". $cat->name ."</a>";
            }
        ?>
    </div>
    <div class="col small-12">
        <div id="show_products_by_filter_category"></div>
    </div>
</div>