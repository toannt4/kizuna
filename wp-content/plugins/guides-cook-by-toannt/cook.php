<?php
/*
 
Plugin Name: Guides cook by toannt
 
Plugin URI: https://solashi.com
 
Description: Create guides cook by toannt
 
Version: 1.0.0
 
Author: Toannt@solashi.com
 
Author URI: https://facebook.com/toan.ngotien.3
 
License: GPLv2 or later
 
Text Domain: Guides cook by toannt
 
*/

function _create_post_type(){
    register_post_type( 'cook',
    // CPT Options
    array(
        'labels' => array(
        'name' => __( 'Cook' ),
        'singular_name' => __( 'Cook' )
        ),
        'public' => true,
        'menu_icon' => 'dashicons-welcome-add-page',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'taxonomies'  => array( 'category' ),
        'rewrite' => array( 'slug' => 'cong-thuc-nau-an' ),
        )
    );
    // Hooking up our function to theme setup
}

function _init_styles() {
    wp_register_style( 'cook_css', plugin_dir_url(__FILE__) . '/css/cook.css');
    wp_enqueue_style( 'cook_css');
}

function _init_scripts() {
    wp_register_script( 'cook_js', plugin_dir_url(__FILE__) . '/js/cook.js', array('jquery'), '1.0', true );
    wp_enqueue_script('cook_js');
}

function _init(){
    _create_post_type();
    _init_scripts();
    _init_styles();
}

function _show_cooks(){
    $args = array(  
        'post_type' => 'cook',
        'post_status' => 'publish',
        'posts_per_page' => 4, 
        'orderby' => 'post_date', 
        'order' => 'ASC'
    );


    
    $loop = new WP_Query( $args ); 
    
    echo "<div class='cooks row row-small'>";
        while ( $loop->have_posts() ) : $loop->the_post(); 

            $id = get_the_ID();
            $url = get_the_permalink();
            $created_at = get_the_date('j F Y', $id);
            $feature_image_url = wp_get_attachment_url(get_post_thumbnail_id($id));
            $title = get_the_title();
            $time_to_cook = get_field("time_to_cook");
            $level_to_cook = get_field("level_to_cook");
            $cooking_png = plugin_dir_url(__FILE__) . "/assets/cooking.png";
            $cooking_time_png = plugin_dir_url(__FILE__) . "/assets/cooking time.png";

            echo "<div class='cook col small-6 medium-3 large-3'>";
                echo "<a href='{$url}'>"; 
                    echo "<img width='100%' src='{$feature_image_url}' class='cook-feature-image'/>";
                    echo "<p class='cook-created-at'>{$created_at}</p>";
                    echo "<p class='cook-title'>{$title}</p>";
                    echo "<div class='cook-levels'>";
                        echo "<span class='cook-level'>";
                            echo "<img src='{$cooking_time_png}' />";
                            echo "<p class='mb-0'>{$time_to_cook}</p>" ;
                        echo "</span>";
                        echo "<span class='cook-level'>";
                            echo "<img src='{$cooking_png}' />";
                            echo "<p class='mb-0'>{$level_to_cook}</p>";
                        echo '</span>';
                    echo "</div>";
                echo "</a>";
            echo "</div>";
        
        endwhile;
    echo "</div>";
}

function _show_feature_cooks(){
    $args = array(  
        'post_type' => 'cook',
        'post_status' => 'publish',
        'posts_per_page' => 4, 
        'orderby' => 'post_date', 
        'order' => 'ASC',
        'meta_key'      => 'is_feature_guide',
        'meta_value'    => true
    );


    
    $loop = new WP_Query( $args ); 
    
    echo "<div class='feature-cooks row row-full-width row-collapse'>";
        while ( $loop->have_posts() ) : $loop->the_post(); 

            $id = get_the_ID();
            $url = get_the_permalink();
            $created_at = get_the_date('j F Y', $id);
            $feature_image_url = wp_get_attachment_url(get_post_thumbnail_id($id));
            $title = get_the_title();
            $time_to_cook = get_field("time_to_cook");
            $level_to_cook = get_field("level_to_cook");
            $cooking_png = plugin_dir_url(__FILE__) . "/assets/cooking.png";
            $cooking_time_png = plugin_dir_url(__FILE__) . "/assets/cooking time.png";

            
            echo "<div class='cook col small-12'>";
                echo "<a href='{$url}'>"; 
                    echo "<img width='100%' src='{$feature_image_url}' class='cook-feature-image mb-0'/>";
                    echo "<div class='cook-detail-absolute'>";
                        echo "<p class='cook-created-at'>{$created_at}</p>";
                        echo "<h2 class='cook-title'>{$title}</h2>";
                        echo "<div class='cook-levels'>";
                            echo "<span class='cook-level'>";
                                echo "<img src='{$cooking_time_png}' />";
                                echo "<p class='mb-0'>{$time_to_cook}</p>" ;
                            echo "</span>";
                            echo "<span class='cook-level'>";
                                echo "<img src='{$cooking_png}' />";
                                echo "<p class='mb-0'>{$level_to_cook}</p>";
                            echo '</span>';
                        echo "</div>";
                    echo "</div>";
                echo "</a>";
            echo "</div>";
        
        endwhile;
    echo "</div>";
}

function _show_banner_cooks(){
    $args = array(  
        'post_type' => 'cook',
        'post_status' => 'publish',
        'posts_per_page' => 4, 
        'orderby' => 'post_date', 
        'order' => 'ASC',
        'meta_key'      => 'is_banner_guide',
        'meta_value'    => true
    );


    
    $loop = new WP_Query( $args ); 
    
    echo "<div class='banner-cooks row row-full-width row-collapse'>";
        while ( $loop->have_posts() ) : $loop->the_post(); 

            $id = get_the_ID();
            $url = get_the_permalink();
            $feature_image_url = wp_get_attachment_url(get_post_thumbnail_id($id));
            $title = get_the_title();
            $short_description = get_the_excerpt();
            $time_to_cook = get_field("time_to_cook");
            $level_to_cook = get_field("level_to_cook");
            $cooking_png = plugin_dir_url(__FILE__) . "/assets/cooking.png";
            $cooking_time_png = plugin_dir_url(__FILE__) . "/assets/cooking time.png";

            echo "<div class='cook col small-12'>";
                echo "<img width='100%' src='{$feature_image_url}' class='cook-feature-image mb-0'/>";
                echo "<div class='cook-detail-absolute'>";
                    echo "<h2 class='cook-title'>{$title}</h2>";
                    echo "<p class='cook-short-description text-left'>{$short_description}</p>";
                    echo "<div class='cook-levels'>";
                        echo "<span class='cook-level'>";
                            echo "<img src='{$cooking_time_png}' />";
                            echo "<p class='mb-0'>{$time_to_cook}</p>" ;
                        echo "</span>";
                        echo "<span class='cook-level'>";
                            echo "<img src='{$cooking_png}' />";
                            echo "<p class='mb-0'>{$level_to_cook}</p>";
                        echo '</span>';
                    echo "</div>";
                    echo "<a class='cook-go' href='{$url}'>Xem chi tiết</a>";
                echo "</div>";
            echo "</div>";
        
        endwhile;
    echo "</div>";
}

add_action( 'init', '_init' );
add_shortcode( 'show_cooks', '_show_cooks');
add_shortcode( 'show_feature_cooks', '_show_feature_cooks');
add_shortcode( 'show_banner_cooks', '_show_banner_cooks');

